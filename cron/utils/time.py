import datetime
import time
from functools import update_wrapper

def ensure_runable(duration):
    def decorator(f):
        def wrapped(*args, **kwargs):
            before = datetime.datetime.now()
            result = f(*args, **kwargs)
            after = datetime.datetime.now()

            diff = after - before
            if diff < duration:
                time.sleep((duration - diff).seconds)

            return result

        return update_wrapper(wrapped, f)

    return decorator

