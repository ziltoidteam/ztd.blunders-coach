import datetime

# Yes, this is real hostname and password, but it's guarded by firewall
class database(object):
    host=None
    dbname='chessdb'
    user='postgres'
    password=None

    connectionString = "%s %s %s %s" % (
        ("host=%s" % host) if host is not None else "",
        "dbname=%s" % dbname,
        "user=%s" % user,
        ("password=%s" % password) if password is not None else ""
    )

class redis(object):
    host='127.0.0.1'
    port='6379'
    db='0'
    uri = "redis://%s" % host

class rabbitmq(object):
    host='localhost'
    uri = "amqp://%s" % host

class cron(object):
    interval = datetime.timedelta(seconds=15)
    active_users = '1 MONTH'
