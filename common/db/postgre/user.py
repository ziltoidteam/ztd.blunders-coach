from common.db.postgre import core

def getUserIdByToken(token):
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute(
            'SELECT user_id from api_tokens WHERE token = %s;'
            , (token,)
        )

        result = connection.cursor.fetchone()
        if result is None:
            return None

        return result[0]

def getUsernameById(user_id):
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute("""
            SELECT username
            FROM users
            WHERE id = %s;
            """, (user_id,)
        )

        result = connection.cursor.fetchone()
        if result is None:
            raise Exception('User with id %d is not exist' % user_id)

        return result[0]

def getActiveUsers(interval):
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute("""
            SELECT u.id
            FROM vw_activities AS act
            INNER JOIN users as u
                ON act.user_id = u.id
            WHERE act.last_activity > NOW() - INTERVAL %s;"""
            , (interval,)
        )

        data = connection.cursor.fetchall()

        users = [username for (username,) in data] #pylint: disable=unused-variable

        return users
