from json import dumps

from common.db.postgre import core

# Message concept is divided into 2 parts.
# First, Metadata, which descibes user_id and type
# Second, Step, where stored body(actual text) and all other data
# One message can be versioned with several steps, where user
# interact with system thruout simple dialog.
# Only one step can be enabled, others are history
# When all steps confirmed(End date exist), message meant to be ended


def getActiveMessagesInfo(user_id):
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute(
          '''SELECT vwcm.id,
                    vwcm.user_id,
                    vwcm.type_id,
                    vwcm.type_args,
                    vwcm.hash_id,
                    vwcm.body,
                    vwcm.options,
                    vwcm.date_start,
                    vwcm.unlocked_packs
             FROM vw_coach_messages AS vwcm
             WHERE vwcm.user_id = %s
             ORDER BY vwcm.id DESC; '''
            , (user_id,)
        )

        response = connection.cursor.fetchall()
        messages = [ {
                'message_id': message_id,
                'user_id': user_id,
                'message_type_id': message_type_id,
                'message_type_args': message_type_args,
                'hash_id': hash_id,
                'body': body,
                'options': options,
                'date_start': date_start,
                'unlocked_packs': unlocked_packs
            }
            for (message_id,
                 user_id,
                 message_type_id,
                 message_type_args,
                 hash_id,
                 body,
                 options,
                 date_start,
                 unlocked_packs) in response
        ]

        return messages

def getMessageTypeIDByName(message_type_name):
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute(
          ''' SELECT cmt.id
              FROM coach_message_type AS cmt
              WHERE cmt.name = %s;'''
          , (message_type_name,)
        )

        response = connection.cursor.fetchall()
        if len(response) != 1:
            raise Exception("Error: Can not get message type by name")
        (message_type_id,) = response[0]

        return message_type_id

def getMessagesByTypeId(user_id, message_type_id):
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute(
          '''SELECT cm.id
             FROM coach_messages AS cm
             WHERE cm.user_id = %s AND
                   cm.type_id = %s;'''
        , (user_id, message_type_id)
        )

        messages = [
            message_id
            for (message_id,) in connection.cursor.fetchall()
        ]

        return messages


def createMessage(user_id, message_type_id, message_type_args):
    with core.PostgreConnection('w') as connection:
        connection.cursor.execute(
          '''INSERT INTO coach_messages(user_id, type_id, type_args)
             VALUES (
                %s,
                %s,
                %s
             )
             RETURNING id;'''
            , (user_id, message_type_id, dumps(message_type_args))
        )

        response = connection.cursor.fetchall()
        if len(response) != 1:
            raise Exception("Error: Can not create a coach message - metadata")
        (message_id,) = response[0]

        return message_id

def __minify_html__(body):
    return " ".join(body.split())

def createMessageStep(message_id, body, options, unlocked_packs = []):
    with core.PostgreConnection('w') as connection:
        connection.cursor.execute(
          '''INSERT INTO coach_message_step(message_id, body, options, unlocked_packs)
             VALUES (%s, %s, %s, %s::jsonb[])
             RETURNING id'''
            , (message_id, __minify_html__(body), options, [ dumps(unlocked) for unlocked in unlocked_packs])
        )

        response = connection.cursor.fetchall()

        if len(response) != 1:
            raise Exception("Error: Can not create a coach message - step")

        return True

    return False

def getMessageSteps(message_id):
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute(
          '''SELECT cms.id,
                    cms.body,
                    cms.options,
                    cms.selected,
                    cms.unlocked_packs
             FROM coach_message_step as cms
             WHERE cms.message_id = %s
             ORDER BY cms.date_start ASC'''
            , (message_id,)
        )

        steps = [
            {
                'message_step_id': message_step_id,
                'body': body,
                'options': options,
                'selected': selected,
                'unlocked_packs': unlocked_packs
            }
            for (message_step_id,
                 body,
                 options,
                 selected,
                 unlocked_packs) in connection.cursor.fetchall()
        ]

        return steps


def selectMessageStep(message_step_id, selected):
    with core.PostgreConnection('w') as connection:
        connection.cursor.execute(
          '''UPDATE coach_message_step
             SET date_finish = NOW(),
                 selected = %s
             WHERE id = %s; '''
            , (selected, message_step_id)
        )

        if connection.cursor.rowcount != 1:
            return False

    return True


def getMessageByHashId(hash_id):
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute(
          '''SELECT cm.user_id,
                    cm.id,
                    (
                        SELECT name
                        FROM coach_message_type AS cmt
                        WHERE cmt.id = cm.type_id
                    ) AS type_name,
                    cm.type_args
             FROM coach_messages AS cm
	     WHERE cm.hash_id = %s'''
            , (hash_id,)
        )

        response = connection.cursor.fetchall()
        messages = [ {
                'user_id': user_id,
                'message_id': message_id,
                'message_type_name': type_name,
                'message_type_args': type_args
            }
            for (user_id, message_id, type_name, type_args) in response
        ]

        if len(messages) != 1:
            return None

        message = messages[0]

        return message

def getMessageTypes():
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute(
          '''SELECT cmt.name
	     FROM coach_message_type as cmt'''
        )

        response = connection.cursor.fetchall()
        message_types = [
            name
            for (name,) in response
        ]

        return message_types

def getUpdatedMessagesBefore(user_id, message_type_id, duration):
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute(
          ''' SELECT * 
              FROM (
                     SELECT 
                       cm.id, 
                       cm.type_id, 
                       cm.type_args, 
                       MAX(
                            CASE WHEN 
                              cms.date_finish IS NOT NULL 
                            THEN cms.date_finish 
                            ELSE cms.date_start 
                       END) AS date_updated 
                       FROM coach_messages AS cm
                       INNER JOIN coach_message_step AS cms 
                         ON cm.id = cms.message_id
                       GROUP BY cm.id 
                       HAVING 
                         cm.user_id = %s AND 
                         cm.type_id = %s
                  ) AS q 
              WHERE 
                (NOW() - q.date_updated) <= INTERVAL %s
              ORDER BY q.date_updated DESC ;
          ''',
          (user_id, message_type_id, duration)
        )

        response = connection.cursor.fetchall()
        messages = [
            {
                'message_id': message_id,
                'message_type_id': message_type_id,
                'message_type_args': message_type_args,
                'date_updated': date_updated 
            }
            for (message_id, message_type_id, message_type_args, date_updated) in response
        ]

        return messages
