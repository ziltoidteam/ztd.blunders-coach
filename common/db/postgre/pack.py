from json import dumps

from common.db.postgre import core

#{ "type_name": (SELECT pt.name FROM pack_type AS pt WHERE pt.id = p.type_id) }
def getFinishedPacksAfter(user_id, date, pack_type_name, pack_type_args):
    with core.PostgreConnection('r') as connection:
        connection.cursor.execute(
          ''' SELECT p.id
              FROM pack_history AS ph
              INNER JOIN packs AS p
                  ON ph.pack_id = p.id
              INNER JOIN pack_type AS pt
                  ON p.type_id = pt.id
              WHERE ph.user_id = %s AND
                    ph.date_finish > %s AND
                    pt.name = %s AND
                    p.type_args = %s AND
                    ph.result = 1;
          ''',
          (user_id, date, pack_type_name, dumps(pack_type_args))
        )

        response = connection.cursor.fetchall()
        messages = [
            pack_id
            for (pack_id,) in response
        ]

        print("Event: %s" % messages)

        return messages
