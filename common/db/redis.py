import redis
import json
from datetime import datetime

from common import const

def main():
    global db #pylint: disable=global-statement

    pool = redis.ConnectionPool(
        host=const.redis.host,
        port=const.redis.port,
        db=const.redis.db
    )
    db = redis.Redis(connection_pool=pool)

def get(key):
    value = db.get(key)
   
    if value is None:
        return None

    return json.loads(value.decode("utf-8"))

def set(key, value, ttl = None):
    db.set(key, json.dumps(value), ex = ttl)

def set_timestamp(key):
    (sec,ms) = db.time()

    set(key, sec)

def time():
    (sec,ms) = db.time()

    return sec

main()
