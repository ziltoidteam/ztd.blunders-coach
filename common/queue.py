from celery import Celery

from common import const

class WorkerQueue:
  celery = None

  def __init__(self):
    self.celery = Celery('workers', backend=const.redis.uri, broker=const.rabbitmq.uri)

  def __enter__(self):
        return self

  def __exit__(self, type, value, traceback):
        pass

  def add(self, name, method, data):
    return self.celery.send_task("worker.%s.%s" % (name, method), args=[data])

