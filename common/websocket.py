from flask_socketio import SocketIO
from flask_socketio import emit
from flask_socketio import join_room, leave_room

from common.db import postgre
from common import const

import eventlet
eventlet.monkey_patch()

# SocketIO can work in 2 modes
# First, as a listener and sender, where it connects to Flask application and serves as server
# In this mode, it uses a broker to create a cluster of several nodes to share metadata. Each client need to be styckied to
# one of the nodes. In the simpliest case, one node is enough.
# Second, as a standalone object, which cannot be used as a server, but can send messages to clients. This mode
# use a broker too to send a message to a server node, which proxies it to client. This mode we will use on worker nodes. 
class WebSocket:
    socketio = SocketIO()

    def __init__(self):
        pass

    def flask(self, app):
        self.socketio.init_app(
            app, 
            async_mode='eventlet', 
            message_queue=const.rabbitmq.uri, 
            channel="coach_channel", 
            logger=True, 
            engineio_logger=True
        )
        return self

    def standalone(self):
        self.socketio = SocketIO(message_queue=const.rabbitmq.uri, channel="coach_channel")
        return self

    def join_room(user_id):
        room = "user-%s" % user_id
        join_room(room)  

    def sendCustom(self, user_id, message):
        room = 'user-%s' % user_id

        self.socketio.emit('client', message, namespace='/chat', room=room)

    def sendMessages(self, user_id):
        message = {
            'status': 'ok',
            'action': '/messages/info',
            'data': {
                'messages': [{
                        'message_id': message['hash_id'],
                        'body': message['body'],
                        'options': message['options']
                    }
                    for message in postgre.message.getActiveMessagesInfo(user_id)
                ]
            }
        }

        self.sendCustom(user_id, message)
