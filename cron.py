from cron.utils import time

from common import const
from common.db import postgre
from common.queue import WorkerQueue

queue = WorkerQueue()

def userTasks(user_id):
    message_types = postgre.message.getMessageTypes()
    result = [
        queue.add('cron.%s' % message_type, 'activate', {'user_id': user_id})
        for message_type in message_types
    ]
    return result

@time.ensure_runable(const.cron.interval)
def run():
    users = postgre.user.getActiveUsers(const.cron.active_users)

    tasks = []
    [
       tasks.extend(user_tasks)
       for user_tasks in
       [ 
           userTasks(user_id)
           for user_id in users
       ]
    ]

    # TODO: use groups instead?
    for task in tasks:
        try:
            status = task.get()
            print(status) #debug
        except Exception as e:
            print('Error: task failed for unknown reason')



def main():
    while(True):
        try:
            run()
        except KeyboardInterrupt as e:
            print("Bye.")
            return;
        except Exception as e:
            print(e)
            print('Error: cron job failed to run')

if __name__ == "__main__":
    main()
