
from worker import celery, websocket

@celery.task()
def get(data):
    try:
        user_id = data['user_id']
    except Exception:
        print('Error while parsing task') #TODO: emit to user?
        return;

    websocket.sendMessages(user_id)
