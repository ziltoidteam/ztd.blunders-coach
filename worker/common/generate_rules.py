import datetime
import random

from common.db import postgre
from common.db import redis

# This class acts as simple firewall, aka iptables
# It have following built in rules:
#  * Unique message: triggers when message with given type already triggered in the past for this user
#                    this rules should ensure message is triggered only once
#  * Duplicate active messages: triggers when message with given type is already active for user
#                    should prevent situations, where user have same messages in the same time
#  * Recently updated: triggers when message was changed/updated in the near past.
#                      This rule should ensure we do not flood user with same messages all the time.
#                      If you solved some supported by message challenge, you probably do not want 
#                      make same test for some time. 
#  * 

class GenerateRules:
    user_id = None
    message_type_id = None
    debug = None

    exit = None

    def __init__(self, user_id, message_type_id, debug = False):
        self.user_id = user_id
        self.message_type_id = message_type_id
        self.debug = debug

    def __print__(self, message):
        if self.debug:
            print(message)

    def deny(self, rule, args = {}):
        if not self.exit is None:
            return self

        if rule == 'Unique message':
            status = self.__absolute_exist__()
        elif rule == 'Duplicate active messages':
            status = self.__active_exist__()
        elif rule == 'Probabilistic check':
            status = not self.__probability_check__(args) 
        elif rule == 'Recently updated':
            status = self.__recently_updated_check__(args)
        else:
            raise Exception('Unknown rule received. Cannot parse.')

        if status:
            self.exit = False

        self.__print__('Deny rule: %s -> %s' % (rule, 'Match' if not self.exit is None else 'Passed' ))

        return self

    def default(self, value):
        if not self.exit is None:
            return self.exit

        self.__print__("Deny rule: returning default value: %s" % value)

        return value

    def __absolute_exist__(self):
        messages = postgre.message.getMessagesByTypeId(self.user_id, self.message_type_id)

        return len(messages) > 0

    def __active_exist__(self):
       messages = [ # There would be limited ammount of active messages per user
           message
           for message in postgre.message.getActiveMessagesInfo(self.user_id)
           if message['message_type_id'] ==  self.message_type_id
       ]

       return len(messages) > 0

    def __probability_check__(self, args):
        suppress = 1.0 # basic suppress is nothing

        ### Step 1 - calculate P
        if 'suppress' in args:
            if 'active' in args['suppress']:
                suppress_active = args['suppress']['active']
                messages = postgre.message.getActiveMessagesInfo(self.user_id)
                if len(messages) > len(suppress_active):
                    suppress *= 0
                else:
                    suppress *= suppress_active[len(messages) - 1]

        base = args['probability']
        P = suppress * base

        ### Step 2
        # We use redis to check passed time from previous check
        timestamp = redis.get("timestamp_uid_%s_mtid_%s" % (self.user_id, self.message_type_id));
        redis.set_timestamp("timestamp_uid_%s_mtid_%s" % (self.user_id, self.message_type_id))
        if timestamp is None: # If no previous record, probability is 0
            return False

        # Sometimes, bacause of asyncronious nature, several
        # Equal events can happen simultaniously, and this will trigger
        # division by zero exception.
        seconds = redis.time() - timestamp
        if seconds == 0: # In this case probability is 0
            return False

        interval = datetime.timedelta(seconds = seconds)

        # If last check time is too far away, we limit it
        if interval > args['interval']:
            interval = args['interval']

        N = args['interval'].seconds/interval.seconds

        p = 1 - (1-P) ** (1.0 / N)

        self.__print__("Interval: (%s/%s), probability: (%s,%s), suppression: %s" % (interval, args['interval'], p, P, suppress))
        if random.random() >= p:
            return False

        return True 

    def __recently_updated_check__(self, args):
        duration = args['duration']

        messages = postgre.message.getUpdatedMessagesBefore(self.user_id, self.message_type_id, duration)
        self.__print__("Duration: %s, Num messages: %s" % (duration, len(messages)))

        return len(messages) > 0

