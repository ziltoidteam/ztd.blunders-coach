import datetime
import random

from common.db import postgre
from common.db import redis

from worker import websocket

class MessageType:
    message_type_id = None
    user_id = None

    def __init__(self, user_id):
        self.message_type_id = postgre.message.getMessageTypeIDByName(self.message_type_name)
        self.user_id = user_id

    # Decides if need to trigger creation of new message
    def __generate__(self):
        raise Exception('Virtual method should be overriden')

    # Decides if need meet for requirements to auto close the step
    def __event__(self, message):
        raise Exception('Virtual method should be overriden')

    # Return dialog, which program all user-coach interaction
    def __dialog__(self, message_id, steps):
        raise Exception('Virtual method should be overriden')

    def activate(self):
        # Update existing events if needed
        messages = postgre.message.getActiveMessagesInfo(self.user_id)
        for message in messages:
            if message['message_type_id'] == self.message_type_id:
                if self.__event__(message):
                    # We put '*' for coach select. It is not exist in options,
                    # But have to be taken into account for dialogs
                    self.select(message['message_id'], '*', coach_mode = True)

        # Generate new message
        message_type_args = self.__generate__()
        if not message_type_args is None:
            # Creates new message's metadata, without body, stored in step table
            message_id = postgre.message.createMessage(self.user_id, self.message_type_id, message_type_args)
            status = self.__step__(message_id) # Add custom body to this message

            websocket.sendMessages(self.user_id)
            pass

        return { 'status': 'ok' }

    # Uses a dialog and creates a new step if needed
    # Returns True is no error has been thrown. If so, return False
    def __step__(self, message_id):
        steps = postgre.message.getMessageSteps(message_id)
        for step in steps:
            if step['selected'] is None:
                raise Exception("Error: Can create step only when all previous steps are selected")

        dialog = self.__dialog__(message_id, steps)

        key = [ step['selected'] for step in steps ]
        result =[ variation for variation in dialog if variation['trigger'] == key ]
        if len(result) > 1:
            raise Exception('Invalid dialog - multiple trigger keys')

        if len(result) == 1:
            chosen = result[0]
            return postgre.message.createMessageStep(message_id,
                                                     chosen['body'],
                                                     chosen['options'],
                                                     chosen['unlocked_packs'])
        # Message is completed, no more steps
        return True

    def select(self, message_id, selected, coach_mode = False):
        steps = postgre.message.getMessageSteps(message_id)
        if len(steps) == 0:
            raise Exception('Message in invalid state - no steps')

        history_steps = steps[0:-1]
        current_step = steps[-1]

        for step in history_steps:
            if step['selected'] is None:
                raise Exception('Message in invalid state - only last step must be in opened state')

        if not current_step['selected'] is None:
            return {
                'status': 'error',
                'message': 'Message already selected'
            }

        if coach_mode == False: 
            # It is user, need to validate it's input
            if not selected in current_step['options']:
                return {
                    'status': 'error',
                    'message': 'Invalid selected value received'
                }
        else:
            # Coach mode, validation is not needed
            pass

        status = postgre.message.selectMessageStep(current_step['message_step_id'], selected)
        if not status:
            raise Exception('Failed to select message step')

        if not self.__step__(message_id):
            raise Exception('Failed to increment step')

        websocket.sendMessages(self.user_id)

        return { 'status': 'ok' }





