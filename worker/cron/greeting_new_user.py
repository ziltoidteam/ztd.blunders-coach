
from worker.common.message_type import MessageType
from worker.common.generate_rules import GenerateRules

from common.db import postgre

from worker import celery

class GreetingNewUser(MessageType):
    message_type_name = 'greeting_new_user'

    # Decides if need to trigger creation of new message
    def __generate__(self):
        status = GenerateRules(
            self.user_id, 
            self.message_type_id
        ).deny(
            rule = 'Unique message'
        ).default(True)

        if not status:
            return None

        message_type_args = {}
        return message_type_args


    def __event__(self, message):
        # No external event can change this message, but user confirmation only
        return False

    # Calculates new step for message, if needed.
    # All steps must be with selected answer from user set
    def __dialog__(self, message_id, steps):
        dialog = [
            {
                'trigger': [],
                'body': '''Welcome, %s!<br>
                           I am coach and I will guide you during all your chess trainings.<br>
                           I will analyze your play, I will give hints to improve your style and
                           will guide you during chess challenges.''' % postgre.user.getUsernameById(self.user_id),
                'options': ['Skip', 'Next'],
                'unlocked_packs': []
            } ,
            {
                'trigger': ['Next'],
                'body': '''All puzzles are divided into packs, grouped by some common rule.<br>
                           You can go to Packs tab and download new packs any time.<br>
                           Packs are completelly free and unlimited. To keep things simple,
                           we limit you only in number of simultaneously assigned packs, but that's all!''',
                'options':  ['Skip', 'Next'],
                'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next'],
                 'body': '''When you start, you get personal rating, that will be changed during the game.<br>
                            See it in top right corner of your screen!<br>
                            As long as you correctly solve puzzles, this rating will get up!<br>
                            As long as you improve your play, this rating will stay high!''',
                 'options': ['Skip', 'Next'],
                 'unlocked_packs': []
            },
            {
                  'trigger': ['Next', 'Next', 'Next'],
                  'body': '''Each puzzle have strength rating.<br>
                             Rated around 1200 are very simple puzzles<br>
                             Rated above 2000 are very difficult to solve<br>
                             Are you ready for new challenges?''',
                  'options': ['Yes'],
                  'unlocked_packs': []
            }
        ]

        return dialog

@celery.task()
def activate(data):
    try:
        user_id = data['user_id']
    except Exception:
        print('Error while parsing task') #TODO: emit to user?
        return;

    worker = GreetingNewUser(user_id)

    return worker.activate()



@celery.task()
def select(data):
    try:
        user_id = data['user_id']
        message_id = data['message_id']
        selected = data['selected']
    except Exception:
        print('Error while parsing task') #TODO: emit to user?
        return;

    worker = GreetingNewUser(user_id)

    return worker.select(message_id, selected)
