import datetime

from worker.common.message_type import MessageType
from worker.common.generate_rules import GenerateRules

from common.db import postgre

from worker import celery

class ChallengeMateInN(MessageType):
    message_type_name = 'challenge_mate_in_n'

    # Decides if need to trigger creation of new message
    def __generate__(self):
        status = GenerateRules(
            self.user_id,
            self.message_type_id,
            debug = True
        ).deny(
            rule = 'Duplicate active messages'
        ).deny(
            rule = 'Recently updated',
            args = {
                "duration": '1 WEEK'
            }
        ).deny(
            rule = 'Probabilistic check',
            args = {
                "interval": datetime.timedelta(hours = 1) ,
                "probability": 0.5,
                "suppress": {
                    "active": [1.0, 1.0, 1.0, 1.0]
                }
            }
        ).default(True)

        if not status:
            return None

        message_type_args = {}
        return message_type_args

    def __event__(self, message):
        unlocked_packs = message['unlocked_packs']

        if len(unlocked_packs) == 0:
            return False

        for pack in unlocked_packs:
            pack_type_name = pack['type_name']
            pack_type_args = pack['args'] if 'args' in pack else {}
            if len(postgre.pack.getFinishedPacksAfter(message['user_id'],
                                                      message['date_start'],
                                                      pack_type_name,
                                                      pack_type_args)) == 0:
                return False

        # Event is triggered and message step should be confirmed
        return True

    # Calculates new step for message, if needed.
    # All steps must be with selected answer from user set
    def __dialog__(self, message_id, steps):
        dialog = [
            {
                'trigger': [], #A
                'body': '''Will you take checkmates challenge?<br>
                           You will need to solve packs with consistently increasing strength<br>
                           Let's check how far you can go!
                           ''',
                'options': ['Skip', 'Next'],
                'unlocked_packs': []
            } ,
            {
                'trigger': ['Next'], #B1
                'body': '''Please confirm to begin the test<br>
                           Then go to the Packs tab and download "Mate in 1" pack.<br>
                           I will wait until you complete this pack.''',
                'options':  ['Quit', 'Next'],
                'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next'], #C1
                 'body': '''Please complete Mate in 1 pack to proceed''',
                 'options': [],
                 'unlocked_packs': [{
                     'type_name': "Mate in N",
                     'description': "Mate in 1",
                     'args' : {
                         'N' : 1
                     }
                 }]
            },
            {
                  'trigger': ['Next', 'Next', '*'], #B2
                  'body': '''You successfully promoted to next level<br>
                             Your next challenge is "Mate in 2" pack''',
                  'options': ['Quit', 'Next'],
                  'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next', '*', 'Next'], #C2
                 'body': '''Please complete Mate in 2 pack to proceed''',
                 'options': [],
                 'unlocked_packs': [{
                     'type_name': "Mate in N",
                     'description': "Mate in 2",
                     'args' : {
                         'N' : 2
                     }
                 }]
            },
            {
                  'trigger': ['Next', 'Next', '*', 'Next', '*'], #B3
                  'body': '''You successfully promoted to next level<br>
                             Your next challenge is "Mate in 3" pack''',
                  'options': ['Quit', 'Next'],
                  'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next'], #C3
                 'body': '''Please complete Mate in 3 pack to proceed''',
                 'options': [],
                 'unlocked_packs': [{
                     'type_name': "Mate in N",
                     'description': "Mate in 3",
                     'args' : {
                         'N' : 3
                     }
                 }]
            },
            {
                  'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*'], #B4
                  'body': '''You successfully promoted to next level<br>
                             Your next challenge is "Mate in 3" pack''',
                  'options': ['Quit', 'Next'],
                  'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next'], #C4
                 'body': '''Please complete Mate in 4 pack to proceed''',
                 'options': [],
                 'unlocked_packs': [{
                     'type_name': "Mate in N",
                     'description': "Mate in 4",
                     'args' : {
                         'N' : 4
                     }
                 }]
            },
            {
                  'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*'], #B5
                  'body': '''You successfully promoted to next level<br>
                             Your next challenge is "Mate in 5" pack''',
                  'options': ['Quit', 'Next'],
                  'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next'], #C5
                 'body': '''Please complete Mate in 5 pack to proceed''',
                 'options': [],
                 'unlocked_packs': [{
                     'type_name': "Mate in N",
                     'description': "Mate in 5",
                     'args' : {
                         'N' : 5
                     }
                 }]
            },
            {
                  'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*'], #B6
                  'body': '''You successfully promoted to next level<br>
                             Your next challenge is "Mate in 6" pack''',
                  'options': ['Quit', 'Next'],
                  'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next'], #C6
                 'body': '''Please complete Mate in 6 pack to proceed''',
                 'options': [],
                 'unlocked_packs': [{
                     'type_name': "Mate in N",
                     'description': "Mate in 6",
                     'args' : {
                         'N' : 6
                     }
                 }]
            },
            {
                  'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*'], #B7
                  'body': '''You successfully promoted to next level<br>
                             Your next challenge is "Mate in 7" pack''',
                  'options': ['Quit', 'Next'],
                  'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next'], #C7
                 'body': '''Please complete Mate in 7 pack to proceed''',
                 'options': [],
                 'unlocked_packs': [{
                     'type_name': "Mate in N",
                     'description': "Mate in 7",
                     'args' : {
                         'N' : 7
                     }
                 }]
            },
            {
                  'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*'], #B8
                  'body': '''You successfully promoted to next level<br>
                             Your next challenge is "Mate in 8" pack''',
                  'options': ['Quit', 'Next'],
                  'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next'], #C8
                 'body': '''Please complete Mate in 8 pack to proceed''',
                 'options': [],
                 'unlocked_packs': [{
                     'type_name': "Mate in N",
                     'description': "Mate in 8",
                     'args' : {
                         'N' : 8
                     }
                 }]
            },
            {
                  'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*'], #B9
                  'body': '''You successfully promoted to next level<br>
                             Your next challenge is "Mate in 9" pack''',
                  'options': ['Quit', 'Next'],
                  'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next'], #C9
                 'body': '''Please complete Mate in 9 pack to proceed''',
                 'options': [],
                 'unlocked_packs': [{
                     'type_name': "Mate in N",
                     'description': "Mate in 9",
                     'args' : {
                         'N' : 9
                     }
                 }]
            },
            {
                  'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*'], #B10
                  'body': '''You successfully promoted to next level<br>
                             Your next challenge is "Mate in 10" pack''',
                  'options': ['Quit', 'Next'],
                  'unlocked_packs': []
            },
            {
                 'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next'], #C10
                 'body': '''Please complete Mate in 10 pack to proceed''',
                 'options': [],
                 'unlocked_packs': [{
                     'type_name': "Mate in N",
                     'description': "Mate in 10",
                     'args' : {
                         'N' : 10
                     }
                 }]
            },
            {
                  'trigger': ['Next', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*', 'Next', '*'], #B10
                  'body': '''You just completed the challenge. Congratulations!''',
                  'options': ['Close'],
                  'unlocked_packs': []
            }


        ]

        return dialog

@celery.task()
def activate(data):
    try:
        user_id = data['user_id']
    except Exception:
        print('Error while parsing task') #TODO: emit to user?
        return;

    worker = ChallengeMateInN(user_id)

    return worker.activate()



@celery.task()
def select(data):
    try:
        user_id = data['user_id']
        message_id = data['message_id']
        selected = data['selected']
    except Exception:
        print('Error while parsing task') #TODO: emit to user?
        return;

    worker = ChallengeMateInN(user_id)

    return worker.select(message_id, selected)
