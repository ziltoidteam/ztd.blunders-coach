##
# Run from root server directory: 
# celery -A worker worker --loglevel=info

import time

from common.websocket import WebSocket
websocket = WebSocket().standalone()

from common.queue import WorkerQueue
celery = WorkerQueue().celery

import worker.messages
import worker.cron

