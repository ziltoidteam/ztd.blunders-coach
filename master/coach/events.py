
from flask import request

from .. import websocket

from common.db import postgre

from common.queue import WorkerQueue

from master.utils import session

def messagesInfo(action_args):
    data = {
        'user_id': session.userID() 
    }

    queue = WorkerQueue()
    queue.add('messages', 'get', data)
    return {
        'status': 'ok' 
    };

def messageSelect(action_args):
    try:
        hash_id = action_args['message_id']
        selected = action_args['selected'] 
    except:
        return {
            'status': 'error',
            'message': 'Parameters missed in sent data'
        }

    message = postgre.message.getMessageByHashId(hash_id)
    if message is None:
        return {
            'status': 'error',
            'message': 'Invalid message ID'
        }

    if message['user_id'] != session.userID():
        return {
            'status': 'error',
            'message': 'This message owned by someone else'
        }

    data = {
        'user_id': session.userID(),
        'message_id': message['message_id'],
        'selected': selected
    } 
 
    destination = "cron.%s" % message['message_type_name']

    queue = WorkerQueue()
    queue.add(destination, 'select', data)
    return { 
        'status': 'ok'
    }; 
    

def actionSelector(action, action_args):
    if   action == '/messages/info':
        return messagesInfo(action_args)
    elif action == '/messages/select':
        return messageSelect(action_args)

    return {
        'status': 'error',
        'message': 'Unknown action'
    }

@websocket.socketio.on('connect')
def connected():
    pass
    
@websocket.socketio.on('disconnect')
def disconnect():
    pass

@websocket.socketio.on('coach', namespace='/chat')
@session.tokenize()
def coach_listener(data):
    print("Client connected: %s" % session.socketioID())
    
    try:
        action = data['action']
        action_args = data['args'] if 'args' in data else {}
    except Exception:
        return {
            'status': 'error',
            'message': 'Parameters missed in sent data'
        }

    if not type(action_args) is dict:
        return {
            'status': 'error',
            'message': 'Invalid args parameter sent'
        }

    return actionSelector(action, action_args)

