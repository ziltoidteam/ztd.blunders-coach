from flask import request

from common.websocket import WebSocket

from common.db import postgre

from functools import update_wrapper

#pylint: disable=too-few-public-methods
class State:
    token = None
    username = None
    userID = None
    socketioID = None

    def __init__(self):
        raise Exception("State is static class, can't call State.__init__")

    def authorize(token): #pylint: disable=no-self-argument
        userId = postgre.user.getUserIdByToken(token) 
        if userId is None:
            return False

        State.token = token
        State.userID = userId
        State.username = postgre.user.getUsernameById(State.userID)
        State.socketioID = request.sid

        WebSocket.join_room(State.userID)

        return True

    #pylint: disable=no-method-argument
    def clean():
        State.token = None
        State.userID = None
        State.username = None
        State.socketioID = None

def tokenize():
    def decorator(f):
        def wrapped(*args, **kwargs):
            (data,) = args
            try:
                token = data['token']
            except Exception:
                return {
                    'status': 'error',
                    'message': 'API token is required'
                }

            try:
                success = State.authorize(token)
                if not success:
                    return {
                        'status': 'error',
                        'message': 'Invalid API token'
                    }

                result = f(*args, **kwargs)
            except Exception as e:
                print(e) # Useful debug printing
                return {
                    'status': 'error',
                    'message': 'Unknown API error'
                }

            State.clean()

            return result

        return update_wrapper(wrapped, f)

    return decorator

def username():
    if not State.token or not State.username:
        raise Exception('Session data in illegal state')

    return State.username


def userID():
    if not State.token or not State.userID:
        raise Exception('Session data in illegal state')
    
    return State.userID

def socketioID():
    if not State.token or not State.userID:
        raise Exception('Session data in illegal state') 

    return State.socketioID
