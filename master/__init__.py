from flask import Flask

from common.websocket import WebSocket

websocket = WebSocket()

def create_app(debug=False):
    """Create an application."""
    app = Flask(__name__)
    app.debug = debug
    #app.config['SECRET_KEY'] = 'gjr39dkjn344_!67#'
    try:
        with open('secret.key', 'rb') as keyFile:
            app.secret_key = keyFile.read()
    except Exception as e:
        print(e)

        print(
            "\nIn order to use sessions you have to set a secret key.\n" +
            "\nYou must generate it and put as file secret.key in root directory\n" +
            "misc/generateSecretKey.py can generate good key for you"
        )
        exit(1)

    from .coach import main as coach_blueprint
    app.register_blueprint(coach_blueprint)

    websocket.flask(app)
    return app
