#!/bin/env python
from master import create_app, websocket

app = create_app(debug=True)

if __name__ == '__main__':
    websocket.socketio.run(app, host='0.0.0.0', port=8090)
